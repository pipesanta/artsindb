"use strict";

const rootRoute = "authors";
const AuthorController = require("../controllers/AuthorController");
const controller=new AuthorController();
// const  { isAuth } = require("../middlewares/auth");

module.exports = {
  applyRoutes: (app) => {
      console.log(controller.tableName)
    app.route(`/${rootRoute}`).get(controller.findAllFromTable.bind(controller)); // search all users
    app.route(`/${rootRoute}/insertOne`).post(controller.insertOne.bind(controller));
    app.route(`/${rootRoute}/deleteOneById`).post(controller.deleteOneByKey.bind(controller)); 
    app.route(`/${rootRoute}/searchResultsByCountry`).get(controller.searchWithJoin.bind(controller));  // search by country
    app.route(`/${rootRoute}/searchOneById`).get(controller.searchByOneId.bind(controller)); // busca un registro
    app.route(`/${rootRoute}/prueba`).get(controller.searchWithJoin.bind(controller));
    app.route(`/${rootRoute}/updateOne`).post(controller.updateOneById.bind(controller));
    // app.route(`/${rootRoute}/find-one`).get(controller.findOneUser);
  }
  
};