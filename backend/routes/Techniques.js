"use strict";

const rootRoute = "techniques";
const TechniqueController = require("../controllers/TechniqueController");
const controller=new TechniqueController();
// const  { isAuth } = require("../middlewares/auth");

module.exports = {
  applyRoutes: (app) => {
      console.log(controller.tableName)
    app.route(`/${rootRoute}`).get(controller.findAllFromTable.bind(controller)); // search all users
    app.route(`/${rootRoute}/insertOne`).post(controller.insertOne.bind(controller)); // search all users
    app.route(`/${rootRoute}/deleteOneById`).post(controller.deleteOneByKey.bind(controller)); // delete an user
    app.route(`/${rootRoute}/deleteOneById`).post(controller.deleteOneByKey.bind(controller));
    app.route(`/${rootRoute}/updateOne`).post(controller.updateOneById.bind(controller)); // update an user
  }
  
};