"use strict";

const rootRoute = "styles";
const StyleController = require("../controllers/StyleController");
const controller=new StyleController();
// const  { isAuth } = require("../middlewares/auth");

module.exports = {
  applyRoutes: (app) => {
      // console.log(controller.tableName)
    app.route(`/${rootRoute}`).get(controller.findAllFromTable.bind(controller)); // search all users
    app.route(`/${rootRoute}/insertOne`).post(controller.insertOne.bind(controller)); // insert an style
    app.route(`/${rootRoute}/deleteOneById`).post(controller.deleteOneByKey.bind(controller)); // delete an user
    app.route(`/${rootRoute}/updateOne`).post(controller.updateOneById.bind(controller)); // update an user
    // app.route(`/${rootRoute}/find-one`).get(controller.findOneUser);
  }
  
};