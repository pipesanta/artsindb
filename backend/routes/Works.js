"use strict";

const rootRoute = "artworks";
const WorkController = require("../controllers/WorkController");
const controller = new WorkController();
// const  { isAuth } = require("../middlewares/auth");

module.exports = {
    applyRoutes: (app) => {
        app.route(`/${rootRoute}`).get(
            controller.searchByFilters.bind(controller)
        ); // search all users
        app.route(`/${rootRoute}/updateOne`).post(
            controller.updateOneById.bind(controller)
        );
        app.route(`/${rootRoute}/insertOne`).post(
            controller.insertOne.bind(controller)
        );
        app.route(`/${rootRoute}/searchOneById`).get(
            controller.searchByOneId.bind(controller)
        );
        app.route(`/${rootRoute}/deleteOne`).post(
            controller.deleteOneByKey.bind(controller)
        );
    },
};
