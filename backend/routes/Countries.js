"use strict";

const rootRoute = "countries";
const CountryController = require("../controllers/CountryController");
const controller=new CountryController();
// const  { isAuth } = require("../middlewares/auth");

module.exports = {
  applyRoutes: (app) => {
      console.log(controller.tableName)
    app.route(`/${rootRoute}`).get(controller.findAllFromTable.bind(controller)); // search all users
    // app.route(`/${rootRoute}/find-one`).get(controller.findOneUser);
  }
  
};