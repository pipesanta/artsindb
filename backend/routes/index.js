"use strict";

const AuthorizationRoutes = require("./Authorization");
const Authors = require("./Authors");
const Works = require("./Works");
const Techniques = require("./Techniques");
const Styles = require("./Styles");
const Countries = require("./Countries")

module.exports = {
  /**
   * @returns {AlarmRoutes}
   */
  Works,
  AuthorizationRoutes,
  Authors,
  Techniques,
  Styles,
  Countries
};
