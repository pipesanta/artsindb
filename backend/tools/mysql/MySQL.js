'use strict'

const { of, Observable } = require("rxjs");
const { map } = require("rxjs/operators");

const MySQL = require("mysql");
const { json } = require("body-parser");

let instance= null;

class MySqlDB{
    constructor( host, user, password, database ){
        this.host = host;
        this.user = user;
        this.password = password;
        this.database = database;
        this.connection =null;
    }

    static  buildParams  (validParams, paramNames, paramValues, bodyParams ) {

        validParams.forEach(p => {
            if( bodyParams[p] !== undefined ){
                paramNames = paramNames + p + ", ";
                const paramValue = bodyParams[p];
    
                paramValues = ( typeof paramValue === "string" )
                    ? paramValues +  `"${paramValue}", `
                    : paramValues + paramValue + ", ";
            }
        });
    
        paramNames = paramNames.substr(0, paramNames.length -2);
        paramValues = paramValues.substr(0, paramValues.length -2);
    
        return { paramNames, paramValues };
    }

    /// METODOS BÁSICOS
    ////////////////////////////////////////

    findAllFromTable$(tableName) {
            return this.executeQuery$(`SELECT * FROM ${tableName}`);
    };

    insertOne$(TABLE_NAME,paramNames,paramValues) {
        return this.executeQuery$(`INSERT INTO ${TABLE_NAME} ( ${paramNames} ) VALUES( ${paramValues} )`);
    };

    saveFields(tableName){
        return this.executeQuery$(`select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = '${tableName}'`);
    }

    deleteOneByKey$(tableName,keyToDelete, keyValueToDelete){
       return this.executeQuery$(`DELETE FROM arte.${tableName} WHERE ${keyToDelete} = '${keyValueToDelete}'`);
    }

     searchByOneId$(tableName, id){
        return this.executeQuery$(`SELECT * 
        FROM ${tableName} WHERE id = '${id}'`);
     }


    searchWithJoin$(idCondition,tablesNames){
        return this.executeQuery$(`SELECT *
        FROM (pais p inner join artista a on a.pais=p.id) WHERE p.id = ${idCountry}`);
     }



     


    start$(){
        console.log("Creando connexion en MySQL");
        return Observable.create( observer => {
            this.connection = MySQL.createConnection({
                host: this.host,
                user: this.user,
                password: this.password,
                database: this.database,
                port: 3306
            });

            this.connection.connect(() => {
                // console.log(this.connection)
                observer.complete();
            });
            
          });
    }

    stop(){
        console.log("cerrando la conexion");
        this.connection.end();
    }

    /**
     * 
     * @param {*} queryAsString 
     * @returns {Observable}
     */
    executeQuery$(queryAsString){
        return Observable.create(observer => {
            // var sql = "INSERT INTO artista (nombre, edad) VALUES ('Juan', 22)";
            console.log("##############\n" + queryAsString + "\n#################");
            this.connection.query(queryAsString, (err, result) => {
                if (err) { 
                    observer.error( err );
                    observer.complete();
                }

                observer.next(result);
                observer.complete();
            });
        });
    }
}

module.exports= {
    MySqlDB,
    /** @returns { MySqlDB } */
    MySQLInstance: () => {
        console.log("Creando instancia de MySQL");
        if(!instance){
            const host =  process.env.MYQSL_HOST || 'localhost';
            const user =  process.env.MYQSL_USER || 'root';
            const password =  process.env.MYQSL_PASSWORD || 'pipe.santa';
            const database =  process.env.MYQSL_DATABASE || 'arte';
            instance = new MySqlDB(host, user, password, database);
        }
        return instance;
    }
    
}

