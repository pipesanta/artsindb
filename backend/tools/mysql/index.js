'use strict'

const {MySQLInstance, MySqlDB} = require("./MySQL");

module.exports= {
    mySQLInstance: MySQLInstance(),
    MySQLDB: MySqlDB
}