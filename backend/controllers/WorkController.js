"use strict";

const { of, observable, bindCallback } = require("rxjs");
const {
    map,
    tap,
    mergeMap,
    switchMap,
    catchError,
    take,
} = require("rxjs/operators");
//const {paramNames, paramValues} = require("functions");

const dateFormat = require("dateformat");
const uuidV4 = require("uuid/v4");
const { mySQLInstance, MySQLDB } = require("../tools/mysql");
const GenericController = require("./GenericController");

const TABLE_NAME = "obra";

class WorkController extends GenericController {
    constructor() {
        super(TABLE_NAME);
    }

    searchByFilters(request, response) {
        // establecer parametros validos

        let queryParams = request.query;
        let filterKeys = Object.keys(queryParams);
        let projections = ["obra.nombre as nombre", "obra.id as id, obra.descripcion as descripcion, obra.imagen as imagen, obra.fecha"];

        // console.log(`Filtros ${JSON.stringify(filterKeys)}`);

        const paramsMapper = {
            artista: {
                getCondition: (artista) => `artista=${artista}`,
                getProjection: (artista) => "",
                getPrimaryKey: `id`,
            },
            estilo: {
                getCondition: (estilo) => `estilo="${estilo}"`,
                getProjection: (estilo) => "",
                getPrimaryKey: "nombre",
            },
            tecnica: {
                getCondition: (tecnica) => `tecnica="${tecnica}"`,
                getProjection: (tecnica) => "",
                getPrimaryKey: "nombre",
            },
            nombre: {
                getCondition: (obra) =>
                    `(UPPER(obra.nombre) LIKE '%${obra.toUpperCase()}%')`,
                getProjection: () => "",
            },
        };

        if (filterKeys.length === 0) {
            super.findAllFromTable(request, response).subscribe();
        } else {
            // ya vamos a tener en cuenta los filtros

            let mySqlQuery = "";
            let whereConditions = [];

            // si tiene solo pais se hac el join con pais
            if (filterKeys.includes("artista") && filterKeys.length == 1) {
                mySqlQuery = `SELECT * FROM (artista inner join obra on obra.artista=artista.id) WHERE artista.id='${queryParams.artista}'`;
            } else if (
                filterKeys.includes("estilo") &&
                filterKeys.length == 1
            ) {
                mySqlQuery = `SELECT * FROM (estilo inner join obra on obra.estilo=estilo.nombre) WHERE estilo.nombre='${queryParams.estilo}'`;
            } else if (
                filterKeys.includes("tecnica") &&
                filterKeys.length == 1
            ) {
                mySqlQuery = `SELECT * FROM (tecnica inner join obra on obra.tecnica=tecnica.nombre) WHERE tecnica.nombre='${queryParams.tecnica}'`;
            } else if (filterKeys.length == 2) {
                filterKeys.forEach((fk) => {
                    //    console.log(`TENEMOS A ${fk} Y SU VALOR ES: ${queryParams[fk]}`);

                    projections.push(paramsMapper[fk].getProjection());
                    whereConditions.push(
                        paramsMapper[fk].getCondition(queryParams[fk])
                    );
                });
                projections = projections.filter((p) => p !== "").join(", ");
                whereConditions = whereConditions.join(" AND ");

                let [tableOne, tableTwo] = [filterKeys[0], filterKeys[1]];

                mySqlQuery = `SELECT ${projections} FROM ( 
                   obra inner join ${tableOne} on ${tableOne}.${paramsMapper[tableOne].getPrimaryKey} = obra.${tableOne}
                   inner join ${tableTwo} on ${tableTwo}.${paramsMapper[tableTwo].getPrimaryKey} = obra.${tableTwo}
                
                )
                WHERE(${whereConditions})`;
            } else {
                filterKeys.forEach((fk) => {
                    //    console.log(`TENEMOS A ${fk} Y SU VALOR ES: ${queryParams[fk]}`);

                    projections.push(paramsMapper[fk].getProjection());
                    whereConditions.push(
                        paramsMapper[fk].getCondition(queryParams[fk])
                    );
                });

                projections = projections.filter((p) => p !== "").join(", ");
                whereConditions = whereConditions.join(" AND ");

                mySqlQuery = `SELECT ${projections} FROM ( 
                      obra inner join artista on obra.artista=artista.id 
                      inner join tecnica on obra.tecnica=tecnica.nombre inner join
                      estilo on obra.estilo=estilo.nombre
                    )
                    WHERE(${whereConditions})`;
            }

            console.log("QUERY CONSTRUIDO ==> ", mySqlQuery);
            return mySQLInstance
                .executeQuery$(mySqlQuery)
                .subscribe((resultado) => {
                    response.status(200).send(resultado);
                });
        }
    }

    insertOne(request,response){
        super.insertOne(request,response,1)
            .subscribe()
    }

    updateOneById(request, response) {
        let updateText = `UPDATE ${this.tableName} SET`;
        let data = request.body;
        let condition = `WHERE id = '${data.id}'`;
        let paramsToUpdate = Object.keys(data)
            .filter((key) => key != "id")
            .map((attribute) =>
                typeof data[attribute] == "string"
                    ? `${attribute} = '${data[attribute]}'`
                    : `${attribute} = ${data[attribute]}`
            )
            .join(", ");
        // update artista set nombre = "maria" where id ="3"*/
        const query = `${updateText} ${paramsToUpdate} ${condition}`;
        console.log("La query que se va a ejecutar es " + query);
        return mySQLInstance
            .executeQuery$(query)
            .pipe(
                // map(authors => authors.map(a => a.RowDataPacket ) ),
                tap((result) => {
                    console.log(result);
                    response.status(200).json(result);
                }),
                catchError((e) => {
                    console.log(e);
                    response.status(502).send(e);
                    return of(null);
                })
            )
            .subscribe();
    }

    searchByOneId(request, response) {
        super.searchOneById$(request,response).subscribe();
    }

    deleteOneByKey(request,response){
        super.deleteOneByKey(request,response).subscribe();
    }
}

module.exports = WorkController;
