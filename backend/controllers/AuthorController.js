"use strict";

const { of, observable, bindCallback } = require("rxjs");
const {
    tap,
    catchError,
} = require("rxjs/operators");
//const {paramNames, paramValues} = require("functions");

const dateFormat = require("dateformat");
const uuidV4 = require("uuid/v4");
const { mySQLInstance, MySQLDB } = require("../tools/mysql");
const GenericController = require("./GenericController");

const TABLE_NAME = "artista";
const AUX_TABLES = ["pais", "obra"];

class AuthorController extends GenericController {
    constructor() {
        super(TABLE_NAME);
    }

    findAllFromTable(request,response){
        // establecer parametros validos


        let queryParams = request.query;
        let filterKeys = Object.keys(queryParams);
        let projections = ["*", "artista.nombre as nombre", "artista.id as id"];

        // console.log(`Filtros ${JSON.stringify(filterKeys)}`);

        const paramsMapper = {
            pais: { 
                join: "inner join pais on pais.id=artista.pais",
                getCondition: (pais) => `pais=${pais}`,
                getProjection: () => ""
            },
            estilo: { 
                join: "inner join estilo on obra.estilo=estilo.nombre",
                getCondition: (estilo) => `estilo="${estilo}"`,
                getProjection: (estilo) => "",
            },
            tecnica: {
                join: "inner join tecnica on obra.tecnica=tecnica.nombre",
                getCondition: (t) => `tecnica="${t}"`,
                getProjection: (t) => "",
            },
            nombre: {
                join: "",
                getCondition: (v) => `(UPPER(artista.nombre) LIKE '%${v.toUpperCase()}%' OR UPPER(artista.apellido) LIKE '%${v.toUpperCase()}%')`,
                getProjection: () => ""
            }

        }

        if(filterKeys.length === 0){
            super.findAllFromTable(request,response).subscribe();
        }else{
            // ya vamos a tener en cuenta los filtros            
            
            
            let mySqlQuery = "";
            let innerJoins = [];
            let whereConditions=[];

             // si tiene solo pais se hac el join con pais
            if(filterKeys.includes("pais") && filterKeys.length==1){
               mySqlQuery = `SELECT * FROM (pais inner join artista on artista.pais=pais.id) WHERE pais.id=${queryParams.pais}`;
            }else{                
               filterKeys.forEach(fk => {
                //    console.log(`TENEMOS A ${fk} Y SU VALOR ES: ${queryParams[fk]}`);
                   
                   projections.push( paramsMapper[fk].getProjection() );
                   innerJoins.push( paramsMapper[fk].join );
                   whereConditions.push( paramsMapper[fk].getCondition( queryParams[fk] ) );
                   
               });

               projections = projections.filter(p => p!=="").join(", ");
               innerJoins = innerJoins.join(" ");
               whereConditions= whereConditions.join(" AND ");




               mySqlQuery = `SELECT ${projections} FROM ( 
                   artista left join obra  on obra.artista=artista.id
                    ${innerJoins}
                )
                WHERE(${whereConditions})`;
            }

            console.log("QUERY CONSTRUIDO ==> ", mySqlQuery);
            return mySQLInstance.executeQuery$(mySqlQuery)
                .subscribe(resultado => {
                    const authorIds = [];
                    let resultadoFiltrado = resultado.filter(a => {
                        if(!authorIds.includes(a.id)){
                            authorIds.push(a.id)
                            return true;
                        }
                        return false;
                    } )
                    response.status(200).send(resultadoFiltrado);
                })
        }
    }



    insertOne(request,response){
        super.insertOne(request, response,1).subscribe();
    }

    deleteOneByKey(request,response){
        super.deleteOneByKey(request,response).subscribe();
    }

    searchByOneId(request,response){
        super.searchOneById$(request,response).subscribe();
    }

    updateOneById(request,response){
        super.updateOneById(request,response).subscribe();
    }

    searchWithJoin(request,response){
        const idsToSearch=[request.query.id1, request.query.id2];
        console.log(idsToSearch);
        return mySQLInstance
        .searchWithJoin$(idToSearch)
        .pipe(
            // map(authors => authors.map(a => a.RowDataPacket ) ),
            tap((result) => {
                console.log(result);
                response.status(200).json(result);
            }),
            catchError((e) => {
                console.log(e);
                response.status(502).send(e);
                return of(null);
            })
        ).subscribe();
    }

    updateOneById(request,response){
        let updateText = `UPDATE ${this.tableName} SET`;
        let data = request.body;
        let condition = `WHERE id = '${data.id}'`;
        let paramsToUpdate = Object.keys(data)
            .filter(key=> key!= 'id')
            .map(attribute => (typeof data[attribute]=='string') 
                ? `${attribute} = '${data[attribute]}'` 
                : `${attribute} = ${data[attribute]}`)
            .join(', ');
            // update artista set nombre = "maria" where id ="3"*/
        const query = `${updateText} ${paramsToUpdate} ${condition}`
        console.log("La query que se va a ejecutar es " + query)
        return mySQLInstance.executeQuery$(query).pipe(
            // map(authors => authors.map(a => a.RowDataPacket ) ),
            tap((result) => {
                console.log(result);
                response.status(200).json(result);
              
            }),
            catchError((e) => {
                console.log(e);
                response.status(502).send(e);
                return of(null);
            })
        )
        .subscribe();
    }

}

module.exports = AuthorController;

/*

  Retorna todos los usuario que cumplan con la condicon en el query
 
exports.findUsers = (req, res) => {
    if (!req) res.send(501);

    const requestParams = req.params || {};

    const requestBody = req.body || {};
    console.log({ requestParams, requestBody });

    mySQLInstance
        .executeQuery$("SELECT * FROM artista")
        .pipe(
            // map(authors => authors.map(a => a.RowDataPacket ) ),
            tap((authors) => {
                console.log(authors);
                res.status(200).json(authors);
            }),
            catchError((e) => {
                console.log(e);
                res.status(502).send(e);
                return of(null);
            })
        )
        .subscribe();
};

exports.insertOne = (req, res) => {
    if (!req) res.send(501);

    const requestParams = req.params || {};
    const requestBody = req.body || {};
    const { nombre } = requestBody;
    console.log({ requestParams, requestBody });

    if (!nombre) {
        res.status(400).send("campo nombre es requerido");
        return;
    }
    const validParams = [
        "nombre",
        "apellido",
        "ano_nacimiento",
        "biografia",
        "pais",
    ];

    let _paramNames = `id, `;
    let _paramValues = `"${uuidV4()}", `;

    const { paramNames, paramValues } = MySQLDB.buildParams(
        validParams,
        _paramNames,
        _paramValues,
        requestBody
    );
    console.log(`INSERT INTO ${TABLE_NAME} ( ${paramNames} ) VALUES( ${paramValues} )`);
    mySQLInstance
        .executeQuery$(
            `INSERT INTO ${TABLE_NAME} ( ${paramNames} ) VALUES( ${paramValues} )`
        )
        .pipe(
            tap((authors) => {
                console.log(authors);
                res.status(200).json(authors);
            }),
            catchError((e) => {
                console.log(e);
                res.status(502).send(e);
                return of(null);
            })
        )
        .subscribe();
};*/