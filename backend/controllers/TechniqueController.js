"use strict";

const { of, observable, bindCallback } = require("rxjs");
const {
    map,
    tap,
    mergeMap,
    switchMap,
    catchError,
    take,
} = require("rxjs/operators");
//const {paramNames, paramValues} = require("functions");

const dateFormat = require("dateformat");
const uuidV4 = require("uuid/v4");
const { mySQLInstance, MySQLDB } = require("../tools/mysql");
const GenericController = require("./GenericController");

const TABLE_NAME = "tecnica";

class TechniqueController extends GenericController {
    constructor() {
        super(TABLE_NAME);
    }

    findAllFromTable(request, response) {
        super.findAllFromTable(request, response).subscribe();
    }

    insertOne(request, response) {
        super.insertOne(request, response, 0).subscribe();
    }

    deleteOneByKey(request, response) {
        super.deleteOneByKey(request, response).subscribe();
    }

    updateOneById(request, response) {
        let updateText = `UPDATE ${this.tableName} SET`;
        let data = request.body;
        let condition = `WHERE nombre = '${data.nombre}'`;
        let paramsToUpdate = Object.keys(data)
            .filter((key) => key != "nombre")
            .map((attribute) =>
                typeof data[attribute] == "string"
                    ? `${attribute} = '${data[attribute]}'`
                    : `${attribute} = ${data[attribute]}`
            )
            .join(", ");
        // update artista set nombre = "maria" where id ="3"*/
        const query = `${updateText} ${paramsToUpdate} ${condition}`;
        console.log("La query que se va a ejecutar es " + query);
        return mySQLInstance
            .executeQuery$(query)
            .pipe(
                // map(authors => authors.map(a => a.RowDataPacket ) ),
                tap((result) => {
                    console.log(result);
                    response.status(200).json(result);
                }),
                catchError((e) => {
                    console.log(e);
                    response.status(502).send(e);
                    return of(null);
                })
            )
            .subscribe();
    }
}

module.exports = TechniqueController;
