"use strict";

const { of, observable, bindCallback } = require("rxjs");
const {
    map,
    tap,
    mergeMap,
    switchMap,
    catchError,
    take,
} = require("rxjs/operators");
//const {paramNames, paramValues} = require("functions");

const dateFormat = require("dateformat");
const uuidV4 = require("uuid/v4");
const { mySQLInstance, MySQLDB } = require("../tools/mysql");
const GenericController = require("./GenericController");

const TABLE_NAME = "pais";

class CountryController extends GenericController {
    constructor() {
        super(TABLE_NAME);
    }

    findAllFromTable(request,response){
        super.findAllFromTable(request,response).subscribe();
    }
   
}

module.exports = CountryController;
