"use strict";

const { of, observable, bindCallback } = require("rxjs");
const {
    map,
    tap,
    mergeMap,
    switchMap,
    catchError,
    take,
    delay,
} = require("rxjs/operators");
    
const dateFormat = require("dateformat");
const uuidV4 = require("uuid/v4");
const { mySQLInstance, MySQLDB } = require("../tools/mysql");
const MySQL = require("../tools/mysql/MySQL");

const getRowReferenceError = (tableName) => {
    switch (tableName) {
        case "artista":
            return "No se puede eliminar este artista porque tiene obras a su nombre"
    
        default:
            return "Este registro no se puede eliminar por ser llave foranea de otra tabla"
    }
}

class GenericController {
    constructor(tableName) {
        this.tableName = tableName;
        this.validFields = [];

        of(null).pipe(
            delay(3000),
            mergeMap(()=> mySQLInstance.saveFields(this.tableName)), 
            map(columns=>columns.map(obj=>obj.COLUMN_NAME)),
            tap(fields=>{
                this.validFields=fields;
            })
           
        ).subscribe();
      
    }

    setTableName(tableName) {
        this.tableName = tableName;
    }

    findAllFromTable(request, response) {
        //if (!request) response.send(501);

        const requestParams = request.params || {};

        const requestBody = request.body || {};

        return mySQLInstance.findAllFromTable$(this.tableName).pipe(
            // map(authors => authors.map(a => a.RowDataPacket ) ),
            tap((result) => {
                response.status(200).json(result);
            }),
            catchError((e) => {
                console.log(e);
                response.status(502).send(e);
                return of(null);
            })
        );
    }

    //auto_id = 1 si la tabla tiene como llave principal un id autogenerado, al contrario
    // darle otro varlo

    insertOne(request, response, auto_id){
        console.log("Los valores finales son " + this.validFields)
        if (!request) {response.send(501)
            return of(null);
        }
        const requestParams = request.params || {};
        const requestBody = request.body || {};
        const { nombre } = requestBody;
    
        if (!nombre) {
            response.status(400).send("campo nombre es requerido");
            return of(null);
        }
        let _paramNames,_paramValues;
        if(auto_id==1){
             _paramNames = `id, `;
             _paramValues = `"${uuidV4()}", `;
        }else{
             _paramNames = '';
             _paramValues = '';
        }
       
    
        const { paramNames, paramValues } = MySQLDB.buildParams(
            this.validFields,
            _paramNames,
            _paramValues,
            requestBody
        );
        return mySQLInstance.insertOne$(this.tableName,paramNames,paramValues).pipe(
            tap((result) => {
                console.log(result);
                response.status(200).json(result);
            }),
            catchError((e) => {
                if(e.code==='ER_DUP_ENTRY'){
                    response.status(200).send({_code: 'ER_DUP_ENTRY' })
                }else{
                    console.log(e);
                    response.status(502).send(e);

                }
                return of(null);
            })
        )
          
    }

    deleteOneByKey(request,response){

        const primaryKey=Object.keys(request.body)[0];
        const keyValueToDelete=request.body[primaryKey];

        return mySQLInstance.deleteOneByKey$(this.tableName, primaryKey, keyValueToDelete).pipe(
            // map(authors => authors.map(a => a.RowDataPacket ) ),
            tap((result) => {
                response.status(200).json(result);
            }),
            catchError((e) => {
                console.log("ERROR AL INTENTAR ELIMINAR UN REGISTRO  DE LA TABLA", this.tableName );
                console.log(e);
                if(e.code === "ER_ROW_IS_REFERENCED_2"){
                    response.status(200).send({ mensaje: getRowReferenceError(this.tableName), _code: "ERROR_LLAVE_FORANEA" })
                }else{
                    response.status(502).send(e);
                }
                // TODO
                return of(null);
            })
        );

    }

    searchOneById$(request,response){
   
        let idToSearch = request.query.id;

        return mySQLInstance.searchByOneId$(this.tableName, idToSearch).pipe(
            // map(authors => authors.map(a => a.RowDataPacket ) ),
            tap((result) => {
                response.status(200).json(result[0]);
            }),
            catchError((e) => {
                console.log(e);
                response.status(502).send(e);
                return of(null);
            })
        );
    }

    updateOneById(request,response){
   
        let idToUpdate = request.body.id;
        let attributeToUpdate = request.body.att;
        let updateInformation = request.body.info;
        /* IdToUpdate: El registro a modificar
           attribute: El atributo a modificar (ejemplo: el nombre)
           info: Valor nuevo para el atributo del registro
           -- `UPDATE ${attributeToUpdate} SET $ = ${updateInformation} WHERE id = '${idToUpdate}'
           //  { name: "juan", age: 45 }
           object.key(objectoA).map(k => `SET ${k} = objectoA[k]` ) 
        */
        return mySQLInstance.updateOneById$(this.tableName, idToUpdate, attributeToUpdate, updateInformation).pipe(
            // map(authors => authors.map(a => a.RowDataPacket ) ),
            tap((result) => {
                console.log(result);
                response.status(200).json(result[0]);
              
            }),
            catchError((e) => {
                console.log(e);
                response.status(502).send(e);
                return of(null);
            })
        );
    }

}

module.exports = GenericController;
