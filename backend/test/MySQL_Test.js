'use strict'

const { mySQLInstance } = require("../tools/mysql");
const { concat, Observable, throwError, bindCallback, of, bindNodeCallback, defer } = require("rxjs");
const { delay, mergeMap, tap } = require("rxjs/operators");
const uuidV4 = require('uuid/v4');


function test01$(){
    return Observable.create(observer => {

        mySQLInstance.connection.query("SELECT 1 + 1 AS solution", ( error, results, fields) => {
            if (error) throw error;
            console.log("The solution is: ", results[0].solution);
            console.log("UUID ==> ", uuidV4(), uuidV4() );
            observer.complete();
        });

    });

}

function test02$(){
    return Observable.create(observer => {
        var sql = `INSERT INTO artista (id, nombre) VALUES ("${uuidV4()}", "Juan")`;
        mySQLInstance.connection.query(sql, (err, result) => {
            if (err) throw err;
            console.log("1 record inserted");
            observer.complete();
        });
    });

}

function closeConnection$(){
    return Observable.create(obs => {
        mySQLInstance.connection.end();
        console.log("conexion cerrada");
        obs.complete();
    })
}


concat(
    mySQLInstance.start$(),
    test01$(),
    test02$(),

    // al final terminamos la conexion
    closeConnection$()

).subscribe();




