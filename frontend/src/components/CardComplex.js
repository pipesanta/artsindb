import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function RecipeReviewCard(props) {

  const { urlImage, name, description, date, onViewMoreInfoClick, onEditClick, onDeleteClick, onImageClick, artist } = props;

  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => { setExpanded(!expanded); };

  return (
    <Card className={classes.root}>
      <CardHeader
        action={
          <div>
            <IconButton onClick={onEditClick}>
              <EditIcon />
            </IconButton>
            <IconButton onClick={onDeleteClick}>
              <DeleteIcon />
            </IconButton>
          </div>
        }
        title={name}
        subheader={`${date} -- Autor: ${artist}`}
      />
      <CardMedia
        onClick={ onImageClick }
        className={classes.media}
        image={urlImage}
        title="Paella dish"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          { description }
         
        </Typography>
        {/* <Typography variant="body2" color="textSecondary" component="p"> */}
          <div onClick={ onViewMoreInfoClick } > Ver Mas Información </div>         
        {/* </Typography> */}
        
      </CardContent>
      <CardActions disableSpacing>
        {/* <IconButton onClick={onEditClick} >
          <EditIcon />
        </IconButton> */}
        {/* <IconButton aria-label="share">
          <ShareIcon />
        </IconButton> */}
          {/* <IconButton
            className={clsx(classes.expand, {
              [classes.expandOpen]: expanded,
            })}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="show more"
          >
          <ExpandMoreIcon />
        </IconButton> */}
      </CardActions>
      
    </Card>
  );
}