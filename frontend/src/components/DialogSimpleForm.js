import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';

export default function FormDialog(props) {
  const { visible, handleClose, labelName, title, message } = props;
  const [ dialogForm, setDialogForm ] = useState({ });

     const onCloseDialog = (save) => {
      
      handleClose( save ? dialogForm: null);
      setDialogForm({ });
  }

  return (
    <div>
      <Dialog open={visible} onClose={onCloseDialog.bind(this, false)} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">{title}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {message}
          </DialogContentText>
          <TextField
            value={ dialogForm.nombre || "" }
            onChange={ (e) => setDialogForm({ ...dialogForm, nombre: e.target.value }) }
            autoFocus
            margin="dense"
            id="name"
            label= {labelName}
            type="email"
            className="my-6"
            fullWidth
          />

           <TextareaAutosize 
               className="w-full my-6" aria-label="minimum height" rowsMin={3} 
               onChange={ (e) => setDialogForm({ ...dialogForm, descripcion: e.target.value }) }
               placeholder="Escribe acá la descripción"
               value={ dialogForm.descripcion || "" } 
           />

        </DialogContent>
        <DialogActions>
          <Button onClick={onCloseDialog.bind(this, false)}
            
            color="primary">
            Cancelar
          </Button>
          <Button onClick={onCloseDialog.bind(this, true)} 
            disabled={!dialogForm.nombre}
            color="primary">
              Guargar
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}