import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const useStyles = makeStyles((theme) => ({
  form: {
    display: 'flex',
    flexDirection: 'column',
    margin: 'auto',
    width: 'fit-content',
  },
  formControl: {
    marginTop: theme.spacing(2),
    minWidth: 120,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  formControlLabel: {
    marginTop: theme.spacing(1),
  },
}));


export default function MaxWidthDialog(props) {
    const { name, visible, artist, handleClose } = props;
    const [fullWidth, setFullWidth] = React.useState(true);
    const [maxWidth, setMaxWidth] = React.useState('lg');
    const classes = useStyles();

    // useEffect(() => {
    //     console.log({ name, visible, pictureUrl, handleClose });
    // }, [pictureUrl]);

  const handleMaxWidthChange = (event) => {
    setMaxWidth(event.target.value);
  };

  const handleFullWidthChange = (event) => {
    setFullWidth(event.target.checked);
    // <MenuItem value={false}>false</MenuItem>
    //             <MenuItem value="xs">xs</MenuItem>
    //             <MenuItem value="sm">sm</MenuItem>
    //             <MenuItem value="md">md</MenuItem>
    //             <MenuItem value="lg">lg</MenuItem>
    //             <MenuItem value="xl">xl</MenuItem>
  };

  return (
    <React.Fragment>
      <Dialog
        fullWidth={fullWidth}
        maxWidth={maxWidth}
        open={visible}
        onClose={handleClose}
        aria-labelledby="max-width-dialog-title"
      >
        <DialogTitle className="text-center" id="max-width-dialog-title">{(artist||{}).nombre || "" }</DialogTitle>
        {/* <DialogContent> */}
            {/* <div class> */}
              <div className="w-full pl-32 items-center" style={{ textAlign: "-webkit-center" }}>
                <div className="w-1/2">
                  <img style={{ maxHeight: "400px" }} src={(artist||{}).imagen}></img>
                </div>
                
              </div>

              <div className="w-full mt-12 p-32 flex justify-center text-xs">
                { (artist||{}).biografia }
              </div>
            {/* </div> */}
           
             
        {/* </DialogContent> */}
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}