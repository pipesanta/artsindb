// *https://www.registers.service.gov.uk/registers/country/use-the-api*
// import fetch from 'cross-fetch';
import React, { useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import ApiService from '../services/apiRest';

const  Asynchronous = (props) => {
    const { urlSource, fakeData, onSelectionChange, label, uniqueKey="id", selectedValue, labelMapperFn } = props;
    if(!urlSource){
        console.error("urlSource property is required for Autocomplete component");
    }

    const [open, setOpen] = useState(false);
    const [options, setOptions] = useState([]);
    const [ selectedOption, setSelectedOption ] = useState(null);
    const [optionLoaded, setOptionLoaded] = useState(false);
    const loading = open && options.length === 0;

    useEffect(() => {
        let active = true;

        if (!loading) { return undefined; }
          (async () => {
          const response = (fakeData) 
              ? { data: fakeData } 
              : await ApiService().get(urlSource);

          if (active) {
              setOptions(response.data);
          }
        })();

    return () => { active = false; };
    }, [loading]);

    useEffect(() => {
      
      ApiService().get(urlSource)
        .then(result => {
          const vv = result.data.find(e => (e||{})[uniqueKey] == selectedValue)
          if(vv){
            setSelectedOption(vv);
          }
        })
        .catch(e => console.error(e))

    }, [selectedValue]);


  return (
    <Autocomplete
      // id={ `async-autocomplete-${id}` }
      // style={{ width: 300 }}
      open={open}
      onOpen={() => { setOpen(true) }}
      onClose={() => { setOpen(false) }}
      value={ selectedOption }
      getOptionSelected={(option, value) => option[uniqueKey] === value[uniqueKey] }
      onChange={(e,value,r) => { onSelectionChange(value); setSelectedOption(value)} }
      getOptionLabel={ labelMapperFn }
      options={options}
      loading={loading}
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          variant="outlined"
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
        />
      )}
    />
  );
}

export default Asynchronous;