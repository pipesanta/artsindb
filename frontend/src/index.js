import React from 'react';
import ReactDom from 'react-dom';
import './styles.css';
import App from './App';
import { createStore } from 'redux';
import reducer from './store/reducer';
import { Provider } from 'react-redux';

const store = createStore(reducer);

ReactDom.render(<Provider store={store}><App/></Provider>, document.getElementById("root"));
