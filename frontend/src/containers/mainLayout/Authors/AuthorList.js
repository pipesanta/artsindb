import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import ApiService from "../../../services/apiRest";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import AsyncAutocomplete from "../../../components/AutoCompleteAsync";
import TextField from "@material-ui/core/TextField";
import { store } from "react-notifications-component";
import ArtistDialogViewer from "../../../components/DialogArtistViewer";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

const SimpleTable = (props) => {
    const classes = useStyles();
    const [authorList, setAuthorList] = useState([]);
    const [waitingForRqst, setWaitingForRqst] = useState(true);
    const history = useHistory();
    // poner los filtros es usestate
    const [filters, setFilters] = useState({
        pais: null,
        estilo: null,
        tecnica: null,
        nombre: "",
    });
    const [countryList, setCountryList] = useState([]);
    const [artistDialogVisible, setArtistDialogVisible] = useState(false);
    const [selectedArtist, setSelectedArtist] = useState(null);

    useEffect(() => {
        if (waitingForRqst) {
            // ApiService().get("/authors", {}, { headers: { Authorization: `Bearer ${props.token}` } }).then((response) => {
            ApiService()
                .get("/authors")
                .then((response) => {
                    // console.log(response);

                    // if(response.status == 401){
                    //   console.log("quirat ele token");

                    // }
                    console.log(response.data);
                    setWaitingForRqst(false);
                    setAuthorList(response.data);
                })
                .catch((e) => console.log(e));

            ApiService()
                .get("/countries")
                .then((r) => {
                    const data = r.data;
                    setCountryList(data);
                });
        }
    }, []);

    useEffect(() => {
        console.log(`EL NUEVO VALOS DE LOS FILTERS ES  ==> `, filters);

        // console.log("VAMOS A PROBAR EL FILTRO DE ARTISTA");
        // const rootPath = "/authors?";
        // let paramsAsString = Object.keys(filters)
        //     .filter(key => filters[key] != null &&  filters[key] != ""  )
        //     .map(key => `${key}=${filters[key]}`);
        // paramsAsString = paramsAsString.join("&");

        // console.log(` HACIENDO GET A  ${rootPath}${paramsAsString}`);
        const mappedFilters = {};
        Object.keys(filters)
            .filter((k) => filters[k])
            .forEach((vk) => (mappedFilters[vk] = filters[vk]));

        // ApiService().get(`${rootPath}${paramsAsString}`)
        ApiService()
            .get(`/authors`, mappedFilters)
            .then((result) => {
                console.log("RESULTADO ==> ", result);
                setAuthorList(result.data);
            });
    }, [filters]);

    const onActionClick = (id, action, evt) => {
        console.log({ action, id, evt });
       

        switch (action) {
            case "EDIT":
                history.push(`/main/author/${id}`);
                break;
            case "DELETE":
                ApiService()
                    .post("/authors/deleteOneById", { id })
                    .then((r) => {
                        console.log(r);
                        if (r.data._code === "ERROR_LLAVE_FORANEA") {
                            // todo en el servicio de notificaciones mostrar que no se puede eliminar ....
                            console.log(r.data.mensaje);
                            showNotification(r.data.mensaje, true);
                        } else {
                            // todo en el servicio de notificaciones mostrar que se elimino ....
                            const newAuthorList = authorList.filter(
                                (au) => au.id != id
                            );
                            setAuthorList(newAuthorList);
                            showNotification(
                                "Artista eliminado correctamente",
                                false
                            );
                        }
                    })
                    .catch((e) => {
                        console.log(JSON.stringify(e));
                    });

                break;

            default:
                break;
        }

       
    };

    const onCountryChanged = (country) => {
        setFilters({ ...filters, pais: (country || {}).id || null });
    };

    const onStyleChanged = (style) => {
        setFilters({ ...filters, estilo: (style || {}).nombre || null });
    };

    const onTechniqueChanged = (technique) => {
        setFilters({ ...filters, tecnica: (technique || {}).nombre || null });
    };

    const getCountryName = (countryId) => {
        const country = countryList.find((c) => c.id == countryId);
        return (country || {}).nombre || countryId;
    };

    const showNotification = (msg, error) => {
        store.addNotification({
            title: msg,
            message: ` `,
            type: !error ? "success" : "danger",
            insert: "top",
            container: "bottom-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
                duration: error ? 5000 : 6000,
                onScreen: true,
            },
        });
    };

    const onRowSelected = (artist, evt) => {
        setSelectedArtist(artist);
        setArtistDialogVisible(true);
        
    };

    return (
        <div>
            <div className="m-8 w-full">
                <Button variant="contained" color="primary">
                    <Link to="/main/author/new">Registrar Un Artista</Link>
                </Button>
            </div>
            {/* FILTERS */}
            <div className="flex flex-wrap">
                <div className="w-full sm:w-1/3 my-6 px-12">
                    <AsyncAutocomplete
                        label={"Pais"}
                        urlSource={"/countries"}
                        // fakeData={[ { name: "Colombia", id: 1 }, {name: "Congo", id: 2} ]}
                        uniqueKey="id"
                        labelMapperFn={(option) => option.nombre || ""}
                        onSelectionChange={onCountryChanged}
                    />
                </div>
                <div className="w-full sm:w-1/3 my-6 px-12">
                    <AsyncAutocomplete
                        label={"Estilo"}
                        urlSource={"/styles"}
                        // fakeData={[ { nombre: "oleo" }, { name: "estilo 1" } ]}
                        uniqueKey="nombre"
                        labelMapperFn={(option) => (option || {}).nombre || ""}
                        onSelectionChange={onStyleChanged}
                    />
                </div>
                <div className="w-full sm:w-1/3 my-6 px-12">
                    <AsyncAutocomplete
                        label={"Tecnica"}
                        urlSource={"/techniques"}
                        // fakeData={[ { nombre: "puntillismo" }, { nombre: "tecnica 1" } ]}
                        uniqueKey="nombre"
                        labelMapperFn={(option) => (option || {}).nombre || ""}
                        onSelectionChange={onTechniqueChanged}
                    />
                </div>
                <div className="w-full sm:w-1/3 my-6 px-12 ">
                    <TextField
                        className="w-full"
                        id="outlined-basic"
                        label="Nombre del artista"
                        variant="outlined"
                        value={filters.nombre}
                        onChange={(e) =>
                            setFilters({ ...filters, nombre: e.target.value })
                        }
                    />
                </div>
            </div>
            {/* FILTERS */}

            {/* TABLE */}
            <TableContainer component={Paper} >
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Nombres</TableCell>
                            <TableCell align="right">Apellidos</TableCell>
                            <TableCell align="right">Fecha</TableCell>
                            <TableCell align="right">Pais</TableCell>
                            <TableCell align="center">Acciones</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {authorList.map((row, i) => (
                            <TableRow
                                key={i}
                                className="cursor-pointer"
                                onClick={onRowSelected.bind(this, row)}
                            >
                                <TableCell component="th" scope="row">
                                    {" "}
                                    {row.nombre}{" "}
                                </TableCell>
                                <TableCell align="right">
                                    {row.apellido}
                                </TableCell>
                                <TableCell align="right">
                                    {row.ano_nacimiento}
                                </TableCell>
                                <TableCell align="right">
                                    {getCountryName(row.pais)}
                                </TableCell>
                                <TableCell align="center">
                                    <IconButton
                                        color="primary"
                                        component="span"
                                        onClick={(evt) => {
                                            evt.preventDefault();
                                            evt.stopPropagation();
                                            console.log("borrarararara")
                                            onActionClick(row.id, "EDIT");
                                        }}
                                    >
                                        <EditIcon />
                                    </IconButton>

                                    <IconButton 
                                        color="primary"  
                                        component="span" 
                                        onClick={(evt) => {
                                            evt.preventDefault();
                                            evt.stopPropagation();
                                            onActionClick(row.id, "DELETE");
                                        }}
                                       
                                    >
                                        <DeleteIcon />
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            {/* TABLE */}

            <ArtistDialogViewer 
                visible={artistDialogVisible}
                artist={selectedArtist}
                handleClose={setArtistDialogVisible.bind(this, false)}
            ></ArtistDialogViewer>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        token: state.token,
    };
};

const mapDispatchToProps = (dispath) => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(SimpleTable);
