import React, { useEffect, useState, Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import ApiService from "../../../services/apiRest";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import AuthorPicture from "../../../components/CardComplexInteraction";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import AsyncAutocomplete from "../../../components/AutoCompleteAsync";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import { store } from "react-notifications-component";

const useStyles = makeStyles((theme) => ({
    root: {
        "& > *": {
            margin: theme.spacing(1),
        },
    },
}));

const AuthorDetail = (props) => {
    const [authorForm, setAuthorForm] = useState({ nombre: "", apellido: "" });
    const [open, setOpen] = React.useState(false); // snackbar state
    const [snackbarMessage, setSnackbarMessage] = useState("");

    /** DIALOG */
    const [dialogOpen, setDialogOpen] = React.useState(false);
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
    const [urlValueOnDialog, setUrlValueOnDialog] = useState("");

    const handleCloseDialog = (userConfirmation, a, c) => {
        console.log({ userConfirmation, a, c });
        if (userConfirmation) {
            setAuthorForm({ ...authorForm, imagen: urlValueOnDialog });
        }
        setUrlValueOnDialog("");
        setDialogOpen(false);
    };

    useEffect(() => {
        function updateTaskState() {
            const params = props.match.params;
            const { id } = params;
            if (id !== "new") {
                ApiService()
                    .get("/authors/searchOneById", { id })
                    .then((r) => {
                        // console.log("EL USUARIO ENOCNTRADO ES ==> ", r.data);
                        setAuthorForm(r.data);
                    });
            }
        }
        updateTaskState();
    }, []);

    const showNotification = (msg, error) => {
        store.addNotification({
            title: msg,
            message: ` `,
            type: !error ? "success" : "danger",
            insert: "top",
            container: "bottom-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
                duration: error ? 5000 : 6000,
                onScreen: true,
            },
        });
    };

    const handleClose = (event, reason) => {
        if (reason === "clickaway") {
            return;
        }
        setOpen(false);
    };

    const onAuthorFormChanged = (field, evt) => {
        const fieldValue = evt.target.value;
        switch (field) {
            case "nombre":
                setAuthorForm({ ...authorForm, nombre: fieldValue });
                break;
            case "apellido":
                setAuthorForm({ ...authorForm, apellido: fieldValue });
                break;
            case "ano_nacimiento":
                setAuthorForm({ ...authorForm, ano_nacimiento: fieldValue });
                break;
            case "descripcion":
                setAuthorForm({ ...authorForm, descripcion: fieldValue });
                break;
            case "biografia":
                setAuthorForm({ ...authorForm, biografia: fieldValue });
                break;
            default:
                console.error(
                    `El campo ${field} no esta soportado por onAuthorFormChanged`
                );
        }
    };

    const handleOnSaveAuthor = () => {
        // console.log(authorForm)
        // ApiService().post("/authors/updateOne", { ...authorForm })
        //     .then((r) => {
        //         console.log(r)
        //     })
        //     .catch(e => {
        //         console.error(e);
        //     })
        const edit = authorForm.id != undefined;
        const _snackBarMsg = edit
            ? "El Artista ha sido actualizado"
            : "El Artista ha sido creado";

        // setSnackbarMessage(_snackBarMsg);

        if (edit) {
            ApiService()
                .post("/authors/updateOne", authorForm)
                .then((response) => {
                    console.log(response.data);
                    showNotification(
                        "Se ha modificado la información del artista",
                        false
                    );
                });
        } else {
            ApiService()
                .post("/authors/insertOne", authorForm)
                .then((response) => {
                    console.log(response.data);
                    // setOpen(true);
                    setAuthorForm({ ...authorForm, id: response.data.id });
                    showNotification(
                        "Se ha agregado el artista",
                        false
                    );
                });
        }
    };

    const onCountryChanged = (country) => {
        setAuthorForm({ ...authorForm, pais: (country || {}).id || null });
    };

    const classes = useStyles();

    return (
        <div>
            <form className="flex flex-wrap" noValidate autoComplete="off">
                <div className="w-full flex justify-center">
                    <div className="text-center px-4 py-2 m-2">
                        <AuthorPicture
                            name={authorForm.nombre + " " + authorForm.apellido}
                            date={`${authorForm.ano_nacimiento || ""} ${
                                authorForm.pais || ""
                            }`}
                            description={
                                authorForm.descripcion || "descripción corta"
                            }
                            pictureUrl={
                                authorForm.imagen ||
                                "https://i.ytimg.com/vi/x-RQcdnvBAk/hqdefault.jpg"
                            }
                            clickOnMore={() => setDialogOpen(true)}
                        />
                    </div>
                </div>

                <TextField
                    className={`${classes.root} w-full md:w-1/2`}
                    label="Nombre"
                    value={authorForm.nombre || ""}
                    variant="outlined"
                    onChange={onAuthorFormChanged.bind(this, "nombre")}
                />

                <TextField
                    className={`${classes.root} w-full md:w-1/2`}
                    label="Apellido"
                    value={authorForm.apellido || ""}
                    variant="outlined"
                    onChange={onAuthorFormChanged.bind(this, "apellido")}
                />

                <TextField
                    className={`${classes.root} w-full md:w-1/2`}
                    label="ano_nacimiento"
                    value={authorForm.ano_nacimiento || ""}
                    variant="outlined"
                    onChange={onAuthorFormChanged.bind(this, "ano_nacimiento")}
                />

                <div className={`${classes.root} w-full md:w-1/2`}>
                    <AsyncAutocomplete
                        className={`w-full`}
                        label={"Pais"}
                        urlSource={"/countries"}
                        selectedValue={authorForm.pais}
                        // fakeData={[ { name: "Colombia", id: 1 }, { name: "Congo", id: 2} ]}
                        uniqueKey="id"
                        labelMapperFn={(option) => option.nombre || ""}
                        onSelectionChange={onCountryChanged}
                    />
                </div>

                {/* <TextareaAutosize className="w-full m-6 px-12 py-12" aria-label="minimum height" rowsMin={3} rowsMax={5}
                    placeholder="Sin corta descripción" value={authorForm.descripcion || "" }
                    onChange={onAuthorFormChanged.bind(this, 'descripcion')} 
                /> */}

                <TextareaAutosize
                    className="w-full m-6 px-12 py-12"
                    aria-label="minimum height"
                    rowsMin={7}
                    onChange={onAuthorFormChanged.bind(this, "biografia")}
                    placeholder="Sin Biografía"
                    value={authorForm.biografia || ""}
                />
            </form>

            <div className="m-8">
                <Button
                    variant="contained"
                    color="primary"
                    onClick={handleOnSaveAuthor}
                    disabled={!authorForm.nombre}
                >
                    {authorForm.id ? "Guardar Cambios" : "Crear Nuevo usuario"}
                </Button>
            </div>

            <Snackbar
                anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
                open={open}
                autoHideDuration={6000}
                onClose={handleClose}
                message={snackbarMessage}
                action={
                    <Fragment>
                        <Button
                            color="secondary"
                            size="small"
                            onClick={handleClose}
                        >
                            Cerrar
                        </Button>
                        <IconButton
                            size="small"
                            aria-label="close"
                            color="inherit"
                            onClick={handleClose}
                        >
                            <CloseIcon fontSize="small" />
                        </IconButton>
                    </Fragment>
                }
            />

            {/* DIALOGO PARA CONFIRMACION */}
            <Dialog
                fullScreen={fullScreen}
                open={dialogOpen}
                onClose={handleCloseDialog.bind(this, false)}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title">
                    {"Por favor ingresa la url de la imagen del autor"}
                </DialogTitle>
                <DialogContent>
                    {/* <DialogContentText> */}
                    <TextField
                        className={`${classes.root} w-full`}
                        label="url de la imagen"
                        value={urlValueOnDialog}
                        onChange={(e) => setUrlValueOnDialog(e.target.value)}
                        variant="outlined"
                    />
                    {/* </DialogContentText> */}
                </DialogContent>
                <DialogActions>
                    <Button
                        autoFocus
                        onClick={handleCloseDialog.bind(this, false)}
                        color="primary"
                    >
                        Cncelar
                    </Button>
                    <Button
                        onClick={handleCloseDialog.bind(this, true)}
                        color="primary"
                        autoFocus
                    >
                        Aceptar
                    </Button>
                </DialogActions>
            </Dialog>
            {/* DIALOGO PARA CONFIRMACION */}
        </div>
    );
};

export default AuthorDetail;
