import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

// MATERIAL COMPONENTS
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
// CUSTOM COMPONENTS
import Artwork from "../../../components/CardComplex";
import DialogInfo from "../../../components/DialogShowInfo";
import AsyncAutocomplete from "../../../components/AutoCompleteAsync";
import ApiService from "../../../services/apiRest";
import DialogImageViewer from "../../../components/DialogImageViewer";
import { store } from "react-notifications-component";

const DEFAULT_ARTWORK_PICTURE =
    "https://as.com/betech/imagenes/2018/01/15/portada/1516040841_763274_1516041101_noticia_normal.jpg";

const Gallery = () => {
    const [dialogVisible, setDialogVisible] = useState(false);
    const [dialogData, setDialogData] = useState("");
    const [filters, setFilters] = useState({});
    const [artworks, setArtworks] = useState([]);
    const [imageViewerVisible, setImageViewerVisible] = useState(false);
    const [selectedArtwork, setSelectedArtwork] = useState(null);
    const [authorList, setAuthorList] = useState([]);

    const history = useHistory();

    useEffect(() => {
        console.log(filters);
        const mappedFilters = {};
        Object.keys(filters)
            .filter((k) => filters[k] != null)
            .forEach((vk) => (mappedFilters[vk] = filters[vk]));
        console.log(mappedFilters);
        console.log("acabo de imprimirlos mappedFilters");
        ApiService()
            .get(`/artworks`, mappedFilters)
            .then((result) => {
                console.log("RESULTADO ==> ", result);
                setArtworks(result.data);
            });
            
    }, [filters]);

    useEffect(() => {
                  
            ApiService()
                .get("/authors")
                .then((r) => {
                    const data = r.data;
                    setAuthorList(data);
                });
        
    }, []);

    const getAuthorFullName = (authorId) => {
        const author = authorList.find((c) => c.id == authorId);
        const {nombre,apellido} = author || {};
        const fullName = nombre + " " + apellido
        return fullName != " " ? fullName : authorId;
    };

    const showNotification = (msg, error) => {
        store.addNotification({
            title: msg,
            message: ` `,
            type: !error ? "success" : "danger",
            insert: "top",
            container: "bottom-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
                duration: error ? 5000 : 6000,
                onScreen: true,
            },
        });
    };

    const onStyleChanged = (style) => {
        setFilters({ ...filters, estilo: (style || {}).nombre || null });
    };

    const onTechniqueChanged = (technique) => {
        setFilters({ ...filters, tecnica: (technique || {}).nombre || null });
    };
    const onAuthorChanged = (artista) => {
        setFilters({ ...filters, artista: (artista || {}).id || null });
    };

    const onViewArtworkDetails = (artwork) => {
        console.log("ver mas informacion de   la obra ===> ", artwork);
        setDialogData({ title: artwork.nombre, content: artwork.descripcion });
        setDialogVisible(true);
    };

    const onEditArtworkHandler = (artworkId) => {
        history.push("/main/artwork/" + artworkId);
    };

    const onDeleteArtworkHandler = (artworkId) => {
        ApiService()
            .post("/artworks/deleteOne", { id: artworkId })
            .then((result) => {
                showNotification("Borrado con éxito", false);
                const newArtWork = artworks.filter((ar) => ar.id != artworkId);
                setArtworks(newArtWork);
                //console.log(JSON.stringify(artworks) + "HOLAAAAAAAAAAAAAAAAAAAAAAAA");
                console.log(artworkId);
            });
    };

    const OnViewFullImage = (artwork) => {
        setSelectedArtwork(artwork);
        setImageViewerVisible(true);
    };

    return (
        <div>
            {/* FILTERS */}
            <div className="flex flex-wrap">
                <div className="m-8 w-full">
                    <Button variant="contained" color="primary">
                        <Link to="/main/artwork/new">Registrar Una Obra</Link>
                    </Button>
                </div>

                <div className="w-full sm:w-1/3 my-6 px-12">
                    <AsyncAutocomplete
                        label={"Estilo"}
                        urlSource={"/styles"}
                        // fakeData={[ { nombre: "oleo" }, { name: "estilo 1" } ]}
                        uniqueKey="nombre"
                        labelMapperFn={(option) => (option || {}).nombre || ""}
                        onSelectionChange={onStyleChanged}
                    />
                </div>

                <div className="w-full sm:w-1/3 my-6 px-12">
                    <AsyncAutocomplete
                        label={"Tecnica"}
                        urlSource={"/techniques"}
                        // fakeData={[ { nombre: "puntillismo" }, { nombre: "tecnica 1" } ]}
                        uniqueKey="nombre"
                        labelMapperFn={(option) => (option || {}).nombre || ""}
                        onSelectionChange={onTechniqueChanged}
                    />
                </div>

                <div className="w-full sm:w-1/3 my-6 px-12">
                    <AsyncAutocomplete
                        label={"Artista"}
                        urlSource={"/authors"}
                        // fakeData={[ { nombre: "puntillismo" }, { nombre: "tecnica 1" } ]}
                        uniqueKey="id"
                        labelMapperFn={(option) =>
                            (option || {}).nombre +
                                " " +
                                (option || {}).apellido || ""
                        }
                        onSelectionChange={onAuthorChanged}
                    />
                </div>

                {/* <div className="w-full sm:w-1/3 my-6 px-12 ">
                    <TextField className="w-full" id="outlined-basic" label="Nombre del artista" 
                        variant="outlined" 
                        // value={filters.nombre}
                        // onChange={ (e) => setFilters({ ...filters, nombre: e.target.value  }) }
                    />
                </div> */}
            </div>
            {/* FILTERS */}

            {/* GALLERY */}
            <div className="flex flex-wrap">
                {artworks.map((artwork, i) => (
                    <div key={i} className="mx-12 my-12">
                        <Artwork
                            urlImage={artwork.imagen || DEFAULT_ARTWORK_PICTURE}
                            name={artwork.nombre}
                            date={artwork.fecha}
                            artist={getAuthorFullName(artwork.artista)}
                            onViewMoreInfoClick={onViewArtworkDetails.bind(
                                this,
                                artwork
                            )}
                            onEditClick={onEditArtworkHandler.bind(
                                this,
                                artwork.id
                            )}
                            onDeleteClick={onDeleteArtworkHandler.bind(
                                this,
                                artwork.id
                            )}
                            onImageClick={OnViewFullImage.bind(this, artwork)}
                            description={(artwork.descripcion || "").slice(
                                0,
                                200
                            )}
                        />
                    </div>
                ))}
            </div>
            {/* GALLERY */}

            {/* DIALOG */}
            <DialogInfo
                visible={dialogVisible}
                data={dialogData}
                handleClose={setDialogVisible.bind(this, false)}
            />
            {/* DIALOG */}

            <DialogImageViewer
                visible={imageViewerVisible}
                artwork={selectedArtwork}
                handleClose={setImageViewerVisible.bind(this, false)}
            ></DialogImageViewer>
        </div>
    );
};

export default Gallery;
