import React, { useEffect, useState, Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import ApiService from "../../../services/apiRest";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import ArtworkPicture from "../../../components/CardComplexInteraction";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import AsyncAutocomplete from "../../../components/AutoCompleteAsync";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import { store } from "react-notifications-component";

const useStyles = makeStyles((theme) => ({
    root: {
        "& > *": {
            margin: theme.spacing(1),
        },
    },
}));
const DEFAULT_ARTWORK_PICTURE =
    "https://as.com/betech/imagenes/2018/01/15/portada/1516040841_763274_1516041101_noticia_normal.jpg";

const ArtworkDetail = (props) => {
    const [artworkForm, setArtworkForm] = useState({});
    const [open, setOpen] = React.useState(false); // snackbar state
    const [snackbarMessage, setSnackbarMessage] = useState("");

    /** DIALOG */
    const [dialogOpen, setDialogOpen] = React.useState(false);
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
    const [urlValueOnDialog, setUrlValueOnDialog] = useState("");

    const handleCloseDialog = (userConfirmation, a, c) => {
        console.log({ userConfirmation, a, c });
        if (userConfirmation) {
            setArtworkForm({ ...artworkForm, imagen: urlValueOnDialog });
        }
        setUrlValueOnDialog("");
        setDialogOpen(false);
    };

    useEffect(() => {
        function updateTaskState() {
            const params = props.match.params;
            const { id } = params;
            if (id !== "new") {
                ApiService()
                    .get("/artworks/searchOneById", { id })
                    .then((r) => {
                        console.log("EL USUARIO ENOCNTRADO ES ==> ", r.data);
                        setArtworkForm(r.data);
                    });
            }
        }
        updateTaskState();
    }, []);

    const showNotification = (msg, error) => {
        store.addNotification({
            title: msg,
            message: ` `,
            type: !error ? "success" : "danger",
            insert: "top",
            container: "bottom-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
                duration: error ? 5000 : 6000,
                onScreen: true,
            },
        });
    };

    const handleClose = (event, reason) => {
        if (reason === "clickaway") {
            return;
        }
        setOpen(false);
    };

    const onArtworkFormChanged = (field, evt) => {
        const fieldValue = ((evt || {}).target || {}).value;

        switch (field) {
            case "nombre":
                setArtworkForm({ ...artworkForm, nombre: fieldValue });
                break;
            case "fecha": 
                setArtworkForm({ ...artworkForm, fecha: fieldValue });
                break;
            case "apellido":
                setArtworkForm({ ...artworkForm, apellido: fieldValue });
                break;
            case "ano_nacimiento":
                setArtworkForm({ ...artworkForm, ano_nacimiento: fieldValue });
                break;
            case "descripcion":
                setArtworkForm({ ...artworkForm, descripcion: fieldValue });
                break;
            case "biografia":
                setArtworkForm({ ...artworkForm, biografia: fieldValue });
                break;
            case "artista":
                setArtworkForm({
                    ...artworkForm,
                    artista: (evt || {}).id || null,
                });
                break;
            case "estilo":
                setArtworkForm({
                    ...artworkForm,
                    estilo: (evt || {}).nombre || null,
                });
                break;
            case "tecnica":
                setArtworkForm({
                    ...artworkForm,
                    tecnica: (evt || {}).nombre || null,
                });
                break;

            default:
                console.error(
                    `El campo ${field} no esta soportado por onArtworkFormChanged`
                );
        }
    };

    const handleOnSaveArtwork = () => {
        // console.log(artworkForm)
        // ApiService().post("/artworks/updateOne", { ...artworkForm })
        //     .then((r) => {
        //         console.log(r)
        //     })
        //     .catch(e => {
        //         console.error(e);
        //     })
        const edit = artworkForm.id != undefined;
        const _snackBarMsg = edit
            ? "El Artista ha sido actualizado"
            : "El Artista ha sido creado";

        // setSnackbarMessage(_snackBarMsg);
        console.log({ artworkForm });

        if (edit) {
            ApiService()
                .post("/artworks/updateOne", artworkForm)
                .then((response) => {
                    showNotification("Se ha modificado la obra", false);
                });
        } else {
            ApiService()
                .post("/artworks/insertOne", artworkForm)
                .then((response) => {
                    console.log(response.data);
                    //setOpen(true);
                    showNotification("Agregado con éxito", false);
                    setArtworkForm({ ...artworkForm, id: response.data.id });
                });
        }
    };

    const classes = useStyles();

    return (
        <div>
            <form className="flex flex-wrap" noValidate autoComplete="off">
                {/* FOTO DE LA OBRA */}
                <div className="w-full flex justify-center">
                    <div className="text-center px-4 py-2 m-2">
                        <ArtworkPicture
                            name={artworkForm.nombre}
                            date={`${artworkForm.ano_nacimiento || ""} ${
                                artworkForm.pais || ""
                            }`}
                            description={
                                (artworkForm.descripcion || "").slice(0, 100) ||
                                "descripción corta"
                            }
                            pictureUrl={
                                artworkForm.imagen || DEFAULT_ARTWORK_PICTURE
                            }
                            clickOnMore={() => setDialogOpen(true)}
                        />
                    </div>
                </div>

                <TextField
                    className={`${classes.root} w-full sm:w-1/3 my-6 px-12`}
                    required={true}
                    label="Nombre"
                    value={artworkForm.nombre || ""}
                    variant="outlined"
                    onChange={onArtworkFormChanged.bind(this, "nombre")}
                />

                <TextField
                    className={`${classes.root} w-full sm:w-1/3 my-6 px-12`}
                    required={true}
                    label="Fecha de creación"
                    value={artworkForm.fecha || ""}
                    variant="outlined"
                    onChange={onArtworkFormChanged.bind(this, "fecha")}
                />

                <div className="w-full sm:w-1/3 my-6 px-12">
                    <AsyncAutocomplete
                        label={"Estilo"}
                        urlSource={"/styles"}
                        // fakeData={[ { nombre: "oleo" }, { name: "estilo 1" } ]}
                        uniqueKey="nombre"
                        selectedValue={artworkForm.estilo}
                        labelMapperFn={(option) => (option || {}).nombre || ""}
                        onSelectionChange={onArtworkFormChanged.bind(
                            this,
                            "estilo"
                        )}
                    />
                </div>

                <div className="w-full sm:w-1/3 my-6 px-12">
                    <AsyncAutocomplete
                        label={"Tecnica"}
                        urlSource={"/techniques"}
                        // fakeData={[ { nombre: "puntillismo" }, { nombre: "tecnica 1" } ]}
                        uniqueKey="nombre"
                        selectedValue={artworkForm.tecnica}
                        labelMapperFn={(option) => (option || {}).nombre || ""}
                        onSelectionChange={onArtworkFormChanged.bind(
                            this,
                            "tecnica"
                        )}
                    />
                </div>

                <div className="w-full sm:w-1/3 my-6 px-12">
                    <AsyncAutocomplete
                        label={"Artista"}
                        urlSource={"/authors"}
                        // fakeData={[ { nombre: "puntillismo" }, { nombre: "tecnica 1" } ]}
                        uniqueKey="id"
                        selectedValue={artworkForm.artista}
                        labelMapperFn={(option) =>
                            (option || {}).nombre +
                                " " +
                                (option || {}).apellido || ""
                        }
                        onSelectionChange={onArtworkFormChanged.bind(
                            this,
                            "artista"
                        )}
                    />
                </div>

                <TextareaAutosize
                    className="w-full m-6 px-12 py-12"
                    aria-label="minimum height"
                    rowsMin={3}
                    placeholder="Sin corta descripción"
                    value={artworkForm.descripcion || ""}
                    onChange={onArtworkFormChanged.bind(this, "descripcion")}
                />

                {/* <TextareaAutosize className="w-full m-6 px-12 py-12" aria-label="minimum height" rowsMin={7} 
                    
                    onChange={onArtworkFormChanged.bind(this, "biografia")}
                    placeholder="Sin Biografía" value={artworkForm.biografia || "" } 
                /> */}
            </form>

            <div className="m-8">
                <Button
                    variant="contained"
                    color="primary"
                    onClick={handleOnSaveArtwork}
                    disabled={!artworkForm.nombre}
                >
                    {artworkForm.id ? "Guardar Cambios" : "Crear Nuevo usuario"}
                </Button>
            </div>

            <Snackbar
                anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
                open={open}
                autoHideDuration={6000}
                onClose={handleClose}
                message={snackbarMessage}
                action={
                    <Fragment>
                        <Button
                            color="secondary"
                            size="small"
                            onClick={handleClose}
                        >
                            Cerrar
                        </Button>
                        <IconButton
                            size="small"
                            aria-label="close"
                            color="inherit"
                            onClick={handleClose}
                        >
                            <CloseIcon fontSize="small" />
                        </IconButton>
                    </Fragment>
                }
            />

            {/* DIALOGO PARA CONFIRMACION */}
            <Dialog
                fullScreen={fullScreen}
                open={dialogOpen}
                onClose={handleCloseDialog.bind(this, false)}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title">
                    {"Por favor ingresa la url de la imagen del autor"}
                </DialogTitle>
                <DialogContent>
                    {/* <DialogContentText> */}
                    <TextField
                        className={`${classes.root} w-full`}
                        label="url de la imagen"
                        value={urlValueOnDialog}
                        onChange={(e) => setUrlValueOnDialog(e.target.value)}
                        variant="outlined"
                    />
                    {/* </DialogContentText> */}
                </DialogContent>
                <DialogActions>
                    <Button
                        autoFocus
                        onClick={handleCloseDialog.bind(this, false)}
                        color="primary"
                    >
                        Cancelar
                    </Button>
                    <Button
                        onClick={handleCloseDialog.bind(this, true)}
                        color="primary"
                        autoFocus
                    >
                        Aceptar
                    </Button>
                </DialogActions>
            </Dialog>
            {/* DIALOGO PARA CONFIRMACION */}
        </div>
    );
};

export default ArtworkDetail;
