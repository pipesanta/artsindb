import React, { useState, useEffect } from "react";

import TextField from "@material-ui/core/TextField";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";

import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import CheckIcon from "@material-ui/icons/Check";
import Button from "@material-ui/core/Button";

import DialogSimpleForm from "../../../components/DialogSimpleForm";

import ApiService from "../../../services/apiRest";

import { store } from "react-notifications-component";

const StyleList = () => {
    const [filters, setFilters] = useState({});
    const [styleList, setStyleList] = useState([]);
    const [dialogVisible, setDialogVisible] = useState(false);

    // escucha los cambiso de los filtros
    useEffect(() => {
        console.log({ filters });
        // debo actualizar la lista de estilos segun la consulta  http que haga
        // com se llama al backend ???
        ApiService()
            .get("/styles", filters)
            .then((result) => {
                // saco el data
                const resultDAta = result.data;
                setStyleList(resultDAta);
            })
            .catch((e) => {
                console.error(e);
            });
    }, [filters]);

    const showNotification = (msg, error) => {
        store.addNotification({
            title: msg,
            message: ` `,
            type: !error ? "success" : "danger",
            insert: "top",
            container: "bottom-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
                duration: error ? 5000 : 6000,
                onScreen: true,
            },
        });
    };

    const onDeleteStyleClick = (style) => {
        ApiService()
            .post(`/styles/deleteOneById`, { nombre: style.nombre })
            .then((result) => {
                if (result.data._code === "ERROR_LLAVE_FORANEA") {
                    showNotification(
                        "No se puede eliminar: Existe una obra con este estilo",
                        true
                    );
                } else {
                    showNotification("Borrado con éxito", false);
                    const newStyleList = styleList.filter(
                        (st) => st.nombre != style.nombre
                    );
                    setStyleList(newStyleList);
                }

                // setOpen(true);
            });
    };

    const onSaveStyleClick = (style) => {
        ApiService()
            .post("/styles/insertOne", { nombre: style.nombre })
            .then((response) => {});
    };

    const onUpdateStyleClick = (style) => {
        ApiService()
            .post("/styles/updateOne", {
                nombre: style.nombre,
                descripcion: style.descripcion,
            })
            .then((response) => {
                showNotification("Modificada la descripción con éxito", false);
            });
    };

    const onStyleDescriptionChanged = (nombre, event) => {
        const itemToUpdateIndex = styleList.findIndex(
            (s) => s.nombre === nombre
        );
        console.log(itemToUpdateIndex);
        const styleListCopy = styleList.slice();
        styleListCopy[itemToUpdateIndex].descripcion = event.target.value;
        setStyleList(styleListCopy);
    };

    const onChandleCloseDialog = (formValue) => {
        if (formValue) {
            ApiService()
                .post("/styles/insertOne", formValue)
                .then((response) => {
                    if (response.data._code === "ER_DUP_ENTRY") {
                        showNotification(
                            "No se puede agregar: Ya existe ese estilo",
                            true
                        );
                    } else {
                        showNotification("Agregado con éxito", false);
                        setStyleList([formValue, ...styleList]);
                    }
                });
        }

        setDialogVisible(false);
    };

    return (
        <div className="w-full">
            {/* FILTROS */}
            <div className="w-full px-6 flex justify-between">
                <TextField
                    // className={ `${classes.root} w-full md:w-1/2` }
                    label="Estilo"
                    value={filters.style || ""}
                    variant="outlined"
                    onChange={(e) =>
                        setFilters({ ...filters, style: e.target.value })
                    }
                />

                <Button
                    variant="contained"
                    color="secondary"
                    onClick={() => setDialogVisible(true)}
                >
                    Agregar Estilo
                </Button>
            </div>

            <div></div>

            {/* FILTROS */}

            {/* LISTADO  DE ESTILOS */}
            <div className="w-full">
                {/* FILA QUE CONTENDRA LA INFO DE ESTYLO */}
                {styleList.map((styleItem, i) => (
                    <div
                        key={styleItem.nombre}
                        className="w-full p-12 mt-12"
                        style={{
                            border: "solid 2px blue",
                            borderRadius: "30px",
                        }}
                    >
                        <TextField
                            className={`w-3/12 mx-12`}
                            label="Estilo"
                            value={styleItem.nombre}
                            variant="outlined"
                        />

                        <TextareaAutosize
                            className="w-8/12 p-12 ml-12"
                            aria-label="minimum height"
                            rowsMin={2}
                            onChange={onStyleDescriptionChanged.bind(
                                this,
                                styleItem.nombre
                            )}
                            placeholder="Descripción"
                            value={styleItem.descripcion}
                        />

                        <div className="w-3/12">
                            <IconButton
                                color="secondary"
                                onClick={onDeleteStyleClick.bind(
                                    this,
                                    styleItem
                                )}
                            >
                                <DeleteIcon />
                            </IconButton>
                            <IconButton
                                color="primary"
                                onClick={onUpdateStyleClick.bind(
                                    this,
                                    styleItem
                                )}
                            >
                                <CheckIcon />
                            </IconButton>
                        </div>
                    </div>
                ))}
                {/* FILA QUE CONTENDRA LA INFO DE ESTYLO */}
            </div>
            {/* LISTADO  DE ESTILOS */}

            {/* DIALOG */}
            <DialogSimpleForm
                visible={dialogVisible}
                handleClose={onChandleCloseDialog}
                labelName="Nombre del Estilo"
                title="Agregar un nuevo estilo"
                message="  Por favor ingresa el nombre del estilo y su descripcion, por favor tenga en cuenta que no pueden existir mas de un estilo con el mismo nombre."
            />
            {/* DIALOG */}
        </div>
    );
};

export default StyleList;
