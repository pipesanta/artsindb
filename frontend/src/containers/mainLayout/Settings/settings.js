import React from 'react';
import ApiService from '../../../services/apiRest';
import Button from '@material-ui/core/Button';

const Settings = (props) => {

    const handleRemoveAllUsersFomWs8552 = () => {
        ApiService().post('/users/delete-all-from-ws8552', {} ).then(response => {
            console.log(response)
        })
    }

    return (
        <div>
            <Button
                variant="contained"
                color="primary"
                size="large"
                onClick={handleRemoveAllUsersFomWs8552}
            > eliminar todos los usuarios del WS8552
            </Button>
        </div>
    )

}

export default Settings;