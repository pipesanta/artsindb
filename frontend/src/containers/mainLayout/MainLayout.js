import React, { useEffect } from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import BrushIcon from '@material-ui/icons/Brush';
import BubbleChartIcon from '@material-ui/icons/BubbleChart';
import PhotoLibraryIcon from '@material-ui/icons/PhotoLibrary';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import { useHistory } from 'react-router-dom';
import AutorList from './Authors/AuthorList';
import AuthorDetail from './Authors/AuthorDetail';
import Setings from './Settings/settings';
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions';
// import useWebSocket from '../services/webSocketHook';
// import WebSocketService from '../services/webSocketHook';
// import { tap, map } from 'rxjs/operators';
import ReactNotification from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';
import Gallery from './Gallery/Gallery';
import ArtworkDetail from './Gallery/ArtworkDetail';
import { store } from 'react-notifications-component';
import StyleList from './Styles/StyleList';
import TechniqueList from './Techniques/TechniqueList';

// Styles
import { buildStylesWithTheme } from './MainLayoutStyles';

const useStyles = makeStyles((theme) => buildStylesWithTheme(theme));

const MainLayout = (props) => {

  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const history = useHistory();

  const handleDrawerOpen = () => { setOpen(true)};

  useEffect(() =>{
      console.log("on init");
      const restoredToken = localStorage.getItem("AIDB_TOKEN");
      props.onSetToken(restoredToken);
      
      if(!restoredToken && !props.token){
        //history.push("/login");
      }
      // const subscription = WebSocketService().listenMessages$()
      // .subscribe(
      //   (data) => {     
          
      //     const unknow = data.name == 'unknow';
          
      //     store.addNotification({
      //       title: unknow 
      //         ? `Tarjeta desconocida N° <${data.cardId}> está intentando ingresar al sistema`
      //         : `${data.name} ha ingresado al establecimiento`,
      //       message: ` `,
      //       type: !unknow ? "success": "danger",
      //       insert: "top",
      //       container: "top-right",
      //       animationIn: ["animated", "fadeIn"],
      //       animationOut: ["animated", "fadeOut"],
      //       dismiss: {
      //         duration: 12000,
      //         onScreen: true
      //       }
      //     });
      //   },
      //   (error) => { console.log(error) },
      //   () => { console.log("Terminando la susbcription") }
      // )

      return () => {
          // subscription.unsubscribe();
          console.log("on destroy");
      }
      
  }, [])

  const handleClickOnmenuItem= (route) => {
    console.log(`Navegar el item ====> ${route}`);
    switch (route) {
      case 'logout':
        history.push("/login");
        break;
      case 'authors':
        history.push("/main/authors");
        break;
      case 'settings':
        history.push("/main/settings");
        break;
      case "gallery":
        history.push("/main/gallery");
        break;
      case "styles":
        history.push("/main/styles");
        break;
      case "techniques":
        history.push("/main/techniques");
        break;
      default:
        break;
    }
  }

  return (
    // <Router>
      <div className={classes.root}>
        <ReactNotification />
        <CssBaseline />
        <AppBar position="fixed" className={clsx(classes.appBar, { [classes.appBarShift]: open })}>
          <Toolbar>
              <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerOpen}
                edge="start"
                className={clsx(classes.menuButton, open && classes.hide)}
              >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" noWrap>
              Arte en MySQL y React
            </Typography>
          </Toolbar>

        </AppBar>
        
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="left"
          open={open}
          classes={{ paper: classes.drawerPaper }}
        >
          <div className={classes.drawerHeader}>
            <IconButton onClick={() => setOpen(false)}>
              {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
            </IconButton>
          </div>
          <Divider />

          {/* <ListItem button onClick={handleClickOnmenuItem.bind(this, "logout")} >
            <ListItemIcon> <InboxIcon /> </ListItemIcon>
            <ListItemText primary={"Login"} />            
          </ListItem> */}

          <ListItem button onClick={handleClickOnmenuItem.bind(this, "authors")} >
            <ListItemIcon> <PermIdentityIcon /> </ListItemIcon>
            <ListItemText primary={"Artistas"} />
          </ListItem>

          <ListItem button onClick={handleClickOnmenuItem.bind(this, "gallery")} >
            <ListItemIcon> <PhotoLibraryIcon /> </ListItemIcon>
            <ListItemText primary={"Galeria"} />
          </ListItem>

          <ListItem button onClick={handleClickOnmenuItem.bind(this, "styles")} >
            <ListItemIcon> <BrushIcon /> </ListItemIcon>
            <ListItemText primary={"Estilos"} />
          </ListItem>

          <ListItem button onClick={handleClickOnmenuItem.bind(this, "techniques")} >
            <ListItemIcon> <BubbleChartIcon /> </ListItemIcon>
            <ListItemText primary={"Tecnicas"} />
          </ListItem>

          {/* <ListItem button onClick={handleClickOnmenuItem.bind(this, "settings")} >
            <ListItemIcon> <InboxIcon /> </ListItemIcon>
            <ListItemText primary={"Configuraciones"} />
            <Link to={'/main/settings'}>{name}</Link>
          </ListItem> */}

        </Drawer>

        {/* BODY */}
        <main className={clsx(classes.content, { [classes.contentShift]: open })} >
          <div className={classes.drawerHeader} />

          <Switch>
            {/* <Redirect from="/main" to=""></Redirect> */}
            <Redirect exact path="/main" to="/main/authors"/>
            <Route path="/main/authors" render={ (props) => <AutorList { ...props } /> } />  
            <Route path="/main/gallery" render={ (props) => <Gallery { ...props } /> } />
            <Route path="/main/artwork/:id" render={ (props) => <ArtworkDetail { ...props } /> } />
            <Route path="/main/author/:id" render={ (props) => <AuthorDetail { ...props } /> } />  
            <Route path="/main/styles" render={ (props) => <StyleList { ...props } /> } />  
            <Route path="/main/techniques" render={ (props) => <TechniqueList { ...props } /> } />  
            {/* <Route path="/main/styles"  component={ Setings } />  */}

          </Switch>

        </main>
        {/* BODY */}

      </div>
    // </Router>
  );
}

const mapStateToProps = (state) => {
  return {
    token: state.token
  }
}

const mapDispatchToProps = (dispath) => {
  return {
    onSetToken: (token) => dispath({ type: actionTypes.ROOT_SET_TOKEN, token: token })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainLayout);