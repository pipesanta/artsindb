import React, { useState, useEffect } from "react";

import TextField from "@material-ui/core/TextField";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";

import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import CheckIcon from "@material-ui/icons/Check";
import Button from "@material-ui/core/Button";

import DialogSimpleForm from "../../../components/DialogSimpleForm";

import ApiService from "../../../services/apiRest";

import { store } from "react-notifications-component";

const TechniqueList = () => {
    const [filters, setFilters] = useState({});
    const [techniqueList, setTechniqueList] = useState([]); //
    const [dialogVisible, setDialogVisible] = useState(false);

    // escucha los cambiso de los filtros
    useEffect(() => {
        console.log({ filters });
        // debo actualizar la lista de estilos segun la consulta  http que haga
        // com se llama al backend ???
        ApiService()
            .get("/techniques", filters)
            .then((result) => {
                // saco el data
                const resultDAta = result.data;
                setTechniqueList(resultDAta);
            })
            .catch((e) => {
                console.error(e);
            });
    }, [filters]);

    const showNotification = (msg, error) => {
        store.addNotification({
            title: msg,
            message: ` `,
            type: !error ? "success" : "danger",
            insert: "top",
            container: "bottom-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
                duration: error ? 5000 : 6000,
                onScreen: true,
            },
        });
    };

    const onDeleteTechniqueClick = (technique) => {
        ApiService()
            .post(`/techniques/deleteOneById`, { nombre: technique.nombre })
            .then((result) => {
                if (result.data._code === "ERROR_LLAVE_FORANEA") {
                    showNotification(
                        "No se puede eliminar: Existe una obra con esta técnica",
                        true
                    );
                } else {
                    showNotification("Borrado con éxito", true);
                    const newTechniqueList = techniqueList.filter(
                        (tc) => tc.nombre != technique.nombre
                    );
                    setTechniqueList(newTechniqueList);
                }
            });
    };

    const onSaveTechniqueClick = (technique) => {};

    const onUpdateStyleClick = (style) => {
        ApiService()
            .post("/techniques/updateOne", {
                nombre: style.nombre,
                descripcion: style.descripcion,
            })
            .then((response) => {
                showNotification("Modificada la descripción con éxito", false);
            });
    };

    const onTechniqueDescriptionChanged = (nombre, event) => {
        const itemToUpdateIndex = techniqueList.findIndex(
            (s) => s.nombre === nombre
        );
        console.log(itemToUpdateIndex);
        const techniqueListCopy = techniqueList.slice();
        techniqueListCopy[itemToUpdateIndex].descripcion = event.target.value;
        setTechniqueList(techniqueListCopy);
    };

    const onChandleCloseDialog = (formValue) => {
        if (formValue) {
            ApiService()
                .post("/techniques/insertOne", formValue)
                .then((response) => {
                    if (response.data._code === "ER_DUP_ENTRY") {
                        showNotification(
                            "No se puede agregar: Ya existe esa técnica",
                            true
                        );
                    } else {
                        setTechniqueList([formValue, ...techniqueList]);
                        showNotification("Agregado con éxito", false);
                    }
                });
        }

        setDialogVisible(false);
    };

    return (
        <div className="w-full">
            {/* FILTROS */}
            <div className="w-full px-6 flex justify-between">
                <TextField
                    // className={ `${classes.root} w-full md:w-1/2` }
                    label="Técnica"
                    value={filters.technique || ""}
                    variant="outlined"
                    onChange={(e) =>
                        setFilters({ ...filters, technique: e.target.value })
                    }
                />

                <Button
                    variant="contained"
                    color="secondary"
                    onClick={() => setDialogVisible(true)}
                >
                    Agregar TÉCNICA
                </Button>
            </div>

            {/* FILTROS */}

            {/* LISTADO  DE ESTILOS */}
            <div className="w-full">
                {/* FILA QUE CONTENDRA LA INFO DE ESTYLO */}
                {techniqueList.map((techniqueItem, i) => (
                    <div
                        key={techniqueItem.nombre}
                        className="w-full p-12 mt-12"
                        technique={{
                            border: "solid 2px blue",
                            borderRadius: "30px",
                        }}
                    >
                        <TextField
                            className={`w-3/12 mx-12`}
                            label="Estilo"
                            value={techniqueItem.nombre}
                            variant="outlined"
                        />

                        <TextareaAutosize
                            className="w-8/12 p-12 ml-12"
                            aria-label="minimum height"
                            rowsMin={2}
                            onChange={onTechniqueDescriptionChanged.bind(
                                this,
                                techniqueItem.nombre
                            )}
                            placeholder="Descripción"
                            value={techniqueItem.descripcion}
                        />

                        <div className="w-3/12">
                            <IconButton
                                color="secondary"
                                onClick={onDeleteTechniqueClick.bind(
                                    this,
                                    techniqueItem
                                )}
                            >
                                <DeleteIcon />
                            </IconButton>
                            <IconButton
                                color="primary"
                                onClick={onUpdateStyleClick.bind(
                                    this,
                                    techniqueItem
                                )}
                            >
                                <CheckIcon />
                            </IconButton>
                        </div>
                    </div>
                ))}
                {/* FILA QUE CONTENDRA LA INFO DE ESTYLO */}
            </div>
            {/* LISTADO  DE ESTILOS */}

            {/* DIALOG */}
            <DialogSimpleForm
                visible={dialogVisible}
                handleClose={onChandleCloseDialog}
                labelName="Nombre de la Técnica"
                title="Agregar una nueva técnica"
                message="  Por favor ingresa el nombre de la técnica y su descripcion, por favor tenga en cuenta que no pueden existir mas de una técnica con el mismo nombre."
            />
            {/* DIALOG */}
        </div>
    );
};

export default TechniqueList;
