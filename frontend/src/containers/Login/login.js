import React, { useEffect, useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions';
import restApi from '../../services/apiRest';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://sisga.com.co/saga"> SISGA S.A.S </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Login = (props) => {
  const { authValidated } = props;
  const classes = useStyles();
  const history = useHistory();

  const [pass, setPass] = useState("");
  const [username, setUsername] = useState("");
  const [loginFailed, setLoginFailed] = useState(false);

  useEffect(() => {

    

    

  },[]);

  const handleLoginBtn = () => {

    restApi().post("/auth/login", { username, pass })
      .then(res => {
        console.log(res);
        if( (res.data||{}).code == 200 ){
          props.onSetAuthValidated(true);
          props.onSetToken(res.data.token);
          localStorage.setItem("AIDB_TOKEN", res.data.token);
          setLoginFailed(false);
          history.push("/main");
        }
        else{
          console.log("fallo el login");
          
          setLoginFailed(true);
        }
      });

    // axios.post("http://192.168.1.1:7172/auth/login", { username, pass})
    //   .then(r => {
        
    //   })
    
  }


  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          { authValidated ? "Ya esta logeado" : "DEBES INICAR SESSION ANTES" }
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            onChange={(evt) => {setUsername(evt.target.value)}}
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={(evt) => { setPass(evt.target.value) } }
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button
            type="button"
            fullWidth
            variant="contained"
            color="primary"
            onClick={handleLoginBtn}
            className={classes.submit}
          >
            Entrar
          </Button>
          { loginFailed && <div> Usuario y contraseña invalidos </div> }
          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                ¿Olvidaste la contraseña?
              </Link>
            </Grid>
            {/* <Grid item>
              <Link href="#" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid> */}
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}

const mapStateToProps = (state) => {
  return {
    authValidated: state.authValidated
  }
}

const mapDispatchToProps = (dispath) => {
  return {
    onSetAuthValidated: (value) => dispath({ type: actionTypes.SET_AUTH, authValidated: value }),
    onSetToken: (token) => dispath({ type: actionTypes.ROOT_SET_TOKEN, token: token })
  }
}

export default connect(mapStateToProps, mapDispatchToProps )(Login);