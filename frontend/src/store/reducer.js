import * as actionTypes from './actions';

const initialState = {
    authValidated: false,
    token: null
};



const reducer = (state =  initialState, action) => {

    switch (action.type) {
        case actionTypes.SET_AUTH:
            console.log( action )
            return {
                ...state,
                authValidated: action.authValidated
            }
        case actionTypes.ROOT_SET_TOKEN:
            return {
                ...state,
                token: action.token
            }
    
        default:
            return state;
    }
    
    return state;

}

export default reducer;