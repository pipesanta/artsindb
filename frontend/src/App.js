import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from 'react-router-dom';
import Login from './containers/Login/login';
import MainLayout from './containers/mainLayout/MainLayout';

export default function App() {
  return (
    <Router>
        <Switch>
          <Redirect exact path="/" to="/main" />
          {/*  */}
          <Route path="/main" render={ (props) => <MainLayout { ...props } /> } />
          <Route path="/login" render={ (props) => <Login /> }/>
          {/* Redirect unhandled routes */}
          <Route>
            {/* <div>
              { process.env.PAGE_NOT_FOUND_MSG }
            </div> */}
            <Redirect to="/main" />
          </Route>
        </Switch>
    </Router>
  );
}