
import axios from 'axios';

let instance = null;

class ApiRestService {

    constructor(host, port){
        console.log("INSTANCE DEL SERVICIO REST ", host, port)
        this.host = host;
        this.port = port;
        this.config = {};
    }

    get(path, params= {}, options ={}){
        const url = `http://${this.host}:${this.port}`;
        return axios.get(`${url}${path}`, { 
            ...options,
            params: { ...params }
        });
        
    }

    post(path, data={}, config={}){
        
        const url = `http://${this.host}:${this.port}`;
        console.log(`USING POST ${url}${path}`, data);
        return axios.post(`${url}${path}`, data, config)
    }

    get$(path, params= {}, options={ timeout: 1000}){
        const url = `http://${this.host}:${this.port}`;        
        const requestPromise = axios.get(`${url}${path}`, { ...options, params: { ...params }});
        console.log("USING GET ", url+path, data);
        return of(requestPromise).pipe(
            mergeMap((request) => defer(() => request ).pipe(
                catchError(err => of({ data: err, status: 512 }))
            )),
        );
    }

    post$(path, data={}, config){
        const url = `http://${this.host}:${this.port}`;
        const requestPromise = axios.post(`${url}${path}`, data, config);

        return of(requestPromise).pipe(
            mergeMap((request) => defer(() => request ).pipe(
                catchError(err => of({ data: err, status: 512 }))
            ))
        )
    }

}

/**@returns {ApiRestService} */
export default () => {
    if(!instance){
        const host = process.env.API_HOST;
        const port = process.env.API_HOST_PORT;

        instance =  new ApiRestService(host, port);

    }
    return instance;
}

